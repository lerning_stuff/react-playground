import React, { Component } from 'react';
import { Header } from './components/Header';
import { KeyBoard } from './components/KeyBoard';

import './App.css';

let style = {
  height: '100vh'
}

const drumsData = [
  {
    name: 'Clap',
    char: 'A',
    keyCode: 65,
    sound: '../sounds/clap.wav'
  },
  {
    name: 'HitHat',
    char: 'S',
    keyCode: 83,
    sound: '../sounds/hihat.wav'
  },
  {
    name: 'Kick',
    char: 'D',
    keyCode: 68,
    sound: '../sounds/kick.wav'
  },
  {
    name: 'OpenHat',
    char: 'F',
    keyCode: 70,
    sound: '../sounds/openhat.wav'
  },
  {
    name: 'Boom',
    char: 'G',
    keyCode: 71,
    sound: '../sounds/boom.wav'
  },
  {
    name: 'Ride',
    char: 'H',
    keyCode: 72,
    sound: '../sounds/ride.wav'
  },
  {
    name: 'Snare',
    char: 'J',
    keyCode: 74,
    sound: '../sounds/snare.wav'
  },
  {
    name: 'Tom',
    char: 'K',
    keyCode: 75,
    sound: '../sounds/tom.wav'
  },
  {
    name: 'Tink',
    char: 'L',
    keyCode: 76,
    sound: '../sounds/tink.wav'
  }
];

class App extends Component {
  constructor() {
    super();
    this.state = {serverData: {}}
  }

  componentDidMount() {
    this.setState({serverData: drumsData});
  }

  render() {
    return (
      <div className="container" style={style} >
        <Header />
        {this.state.serverData.length > 0 ?
          <KeyBoard drums={this.state.serverData} /> : <h1>Loading...</h1>
        }
      </div>
    );
  }
}

export default App;
