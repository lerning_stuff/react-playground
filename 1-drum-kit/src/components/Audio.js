import React, { Component } from 'react';

export class Audio extends Component {
  render() {
    return (
      <audio data-key={this.props.keyCode} src={this.props.sound}></audio>
    );
  }
}