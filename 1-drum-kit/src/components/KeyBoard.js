import React, { Component } from 'react';
import { Key } from './Key';
import { Audio } from './Audio';

let style = {
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 32%)',
    gridGap: '19px',
    gridTemplateRows: 'repeat(3, 1fr)',
    width: '50%',
    margin: 'auto',
    height: 'calc(100% - 251px)',
    padding: '5% 0'
}

var Keys = (props) => {
  const drums = props.drums;
  const keyItems = drums.map((drum)=>{
    return(<Key
      key = { drum.name.toString() }
      name = { drum.name }
      char = { drum.char }
      keyCode = { drum.keyCode }
    />);
  });

  return (
    <div className="keys" style={ style }>
      { keyItems }
    </div>
  );
}

var Audios = (props) => {
  const audios = props.audios;
  const audioItems = audios.map((audio)=>{
    return(<Audio
      key = { audio.name }
      keyCode = { audio.keyCode }
      sound = { audio.sound }
    />);
  });
  return (audioItems);
}

export class KeyBoard extends Component {
  render() {
    let drums = this.props.drums;
    return ([
      <Keys key='keys' drums={ drums }/>,
      <Audios key='audios' audios={ drums }/>
    ]);
  }
}