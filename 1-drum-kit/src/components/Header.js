import React, { Component } from 'react';
import logo from '../logo.svg';

let appTitle = "Reactive Drum Kit";
let headerStyle = {
    textAlign: 'center',
    backgroundColor: 'rgba(0, 2, 34, 0.8)',
    padding: '50px 20px',
    color: 'white',
    position: 'relative'
}
let headingStyle = { fontSize: '2em' }

export class Header extends Component {
  render() {
    return (
      <header className="heading" style={headerStyle}>
        <img src={logo} className="logo" alt="logo" />
        <h1 style={headingStyle}>{appTitle}</h1>
      </header>
    );
  }
}