import React, { Component } from 'react';

let blue = '#0069b3';
let keyStyle = {
  backgroundColor: blue,
  display: ' grid',
  justifyContent: 'center',
  alignItems: 'center',
  fontSize: '1.4rem',
  overflow: 'hidden',
  padding: '20px',
  border: '1px solid { blue }',
  borderRadius: '10px',
  textAlign: 'center',
  cursor: 'pointer',
  opacity: '0.7'
}

let headingStyle = {
  width: '100%',
  fontSize: '4em',
  fontWeight: '300'
}

let textStyle = {
  fontSize: '1em',
  textTransform: 'lowercase'
}

let getKeyCode = (event) => {
  if (event.constructor.name === 'MouseEvent')
    return event.target.dataset.key;
  else
    return event.keyCode;
}

let playSound = (event) => {
  var keyCode = getKeyCode(event);
  const audio = document.querySelector(`audio[data-key="${keyCode}"]`);
  const key = document.querySelector(`div[data-key="${keyCode}"]`);
  if (!audio) return;

  key.classList.add('playing');
  audio.currentTime = 0;
  audio.play();
  setTimeout(() => {
    key.classList.remove('playing')
  }, 110);
}

export class Key extends Component {

  componentDidMount() {
    window.addEventListener('keydown', playSound);
    const keys = Array.from(document.querySelectorAll('.key'));
    keys.forEach(key => key.addEventListener('click', playSound));
  }

  render() {
    return (
      <div data-key={this.props.keyCode} className="key" style={keyStyle}>
        <h2 style={headingStyle}>{this.props.char}</h2>
        <p style={textStyle} className="sound">{this.props.name}</p>
      </div>
    );
  }
}